
const scores = {
    score: document.querySelector('#scoreNum'),
    eliminated: document.querySelector('#virEliminatedNum'),
    missed: document.querySelector('#virMissedNum')
};

class Game {
    constructor(playground) {
        this.playground = playground;
        this.incubationTime = 2000;
        this.timeMultiplier = 1;
        this.gameRunning = 10;

        document.querySelector('#newGame').addEventListener('click', () => this.newGame());
        document.querySelector('#restart').addEventListener('click', () => this.restart());
    }


    createNewVirus(id) {
        // console.log("NEW VIRUS");
        // console.log(`${this.gameRunning} === ${id}  ${this.gameRunning === id}`);

        if (this.gameRunning === id) {
            const time = this.incubationTime * this.timeMultiplier;

            this.lastVirus = new Virus(this.player, this.playground, time);
            setTimeout(() => this.createNewVirus(id), time);
        }
    }


    newGame() {
        this.resetGame();

        const mouseControl = document.querySelector('#mouseButton').checked;
        this.player = new Player(this, mouseControl);
        const gameOverEl = document.querySelector("#go");
        gameOverEl.style.visibility = "hidden";

        this.gameRunning += 1;

        this.timeMultiplier = mouseControl ? 1 : 2.5;

        this.createNewVirus(this.gameRunning);
    }

    restart() {
        this.newGame()
    }

    gameOver() {
        console.log("--GAME OVER--");
        this.gameRunning += 1;
        const gameOverEl = document.querySelector("#go");
        gameOverEl.style.visibility = "visible";

        this.resetGame();
    }

    resetGame(){
        if(this.lastVirus != undefined) {
            this.lastVirus.destroySelf();
        }

        if (this.player != undefined){
            this.player.destroySelf();
        }
    }

}

class Player {
    constructor(game, mouse) {
        this.game = game;

        this.el = document.querySelector("#ch");
        this.x = 300;
        this.y = 370;
        this.moveSpeed = 20;

        this.score = 0;
        this.eliminated = 0;
        this.missed = 0;

        if (mouse) {
            const mapEl = document.querySelector('#playerArena');
            mapEl.onmousemove = (e) => this.onMouseMove(e);
        } else {
            document.addEventListener('keydown', (e) => this.onKeyDown(e));
            this.updateElPos(this.x, this.y);
        }

        this.updateScore();
    }

    onMouseMove(e) {


        const x = e.clientX;
        const y = e.clientY;

        // console.log(`x: ${x} ; y: ${y}`);

        this.updateElPos(x, y);
    }

    onKeyDown(e) {
        // console.log(e);

        switch (e.code) {
            // key W
            case "KeyW": {
                this.y -= this.moveSpeed;
                break;
            }
            // key S
            case "KeyS": {
                this.y += this.moveSpeed;
                break;
            }
            // key A
            case "KeyA": {
                this.x -= this.moveSpeed;
                break;
            }
            // key D
            case "KeyD": {
                this.x += this.moveSpeed;
                break;
            }
            // key L
            case "KeyL": {
                const el = document.elementFromPoint(this.x, this.y);
                el.click();
                break;
            }
            default:
                break;
        }

        this.updateElPos(this.x, this.y);
        return;
    }

    updateElPos(x, y) {
        // console.log(this.el.offsetWidth);

        const offsetParent = this.el.offsetParent;

        const realX = x - offsetParent.offsetLeft - (this.el.offsetWidth / 2);
        const realY = y - offsetParent.offsetTop - (this.el.offsetHeight / 2);

        const newX = realX;
        const newY = realY;

        this.el.style.top = (newY) + "px";
        this.el.style.left = (newX) + "px";
    }

    virusEliminated() {
        this.score += 10;
        this.eliminated += 1;
        this.updateScore();
    }

    virusMissed() {
        this.score -= 5;
        this.missed += 1;
        this.updateScore();

        if (this.missed >= 6) {
            this.game.gameOver();
        }
    }

    updateScore() {
        for (const [scoreName, scoreEl] of Object.entries(scores)) {
            scoreEl.innerHTML = this[scoreName];
        }

        const overlayEl = document.querySelector('#overlay');
        overlayEl.style.width = (Math.min(this.missed * 100, 600)) + "px";
    }

    destroySelf(){
        this.virusEliminated = () => {};
        this.virusMissed = () => {};
    }
}

class Virus {
    constructor(player, playground, incubationTime) {
        this.player = player;
        this.worldEl = playground;//document.querySelector("#playerArena");
        this.el = document.createElement("div");
        this.el.id = "vir";
        this.active = true;

        this.worldEl.append(this.el);
        this.placeInWorld();

        this.el.addEventListener('click', () => this.onDestroy());

        //START INCUBATION
        setTimeout(() => this.onSuccessfulInfection(), incubationTime);
    }


    placeInWorld(){
        const xRatio = (this.el.offsetWidth / this.el.offsetParent.clientWidth);
        const yRatio = (this.el.offsetHeight / this.el.offsetParent.clientHeight);


        const realX = Math.random() - xRatio;
        const realY = Math.random() - yRatio;

        const newX = Math.min(100 - xRatio, Math.max(0, realX));
        const newY = Math.min(100 - yRatio, Math.max(0, realY));


        this.el.style.top = (newY * 100) + "%";
        this.el.style.left = (newX * 100) + "%";
    }

    onDestroy() {
        if(this.active) {
            // console.log("VIRUS DESTROYED");
            this.player.virusEliminated();
            this.destroySelf()
        }
    }

    onSuccessfulInfection() {
        if(this.active) {
            // console.log("VIRUS INFECTED");
            this.player.virusMissed();
            this.destroySelf()
        }
        return;
    }

    destroySelf() {
        this.el.remove();
        this.active = false;
    }
}

class World {

}

//SETUP THE GAME
const playgroundEl = document.querySelector("#playerArena");
const game = new Game(playgroundEl);