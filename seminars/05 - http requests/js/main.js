// import {xmlToJson} from "xmlToJson.min.js";


const swUrl = "https://swapi.co/api/people/";

// const req = new XMLHttpRequest();
// req.addEventListener("load", subtask01);
// req.open("GET", swUrl);
// req.send();

function processResponseText(responseText) {
    const responseJson = JSON.parse(responseText);
    // console.log(responseJson);

    const parsedData = responseJson.results;
    return processJson(parsedData);
}

function processJson(json) {
    return json.map(char => char.name);
}

function starWarsRenderFunction(parsedData) {
    const ulEl = document.querySelector('#star-wars');

    for (const char of parsedData) {
        const newLi = document.createElement('li');
        newLi.innerHTML = char;

        ulEl.appendChild(newLi);
    }
}

//---- MAINTASK-SUBTASK 01 ----
function subtask01() {
    const data = processResponseText(this.responseText);
    // console.log(data);
    starWarsRenderFunction(data);
}


function myRequest(url) {
    return new Promise(function (resolve, reject) {
        const req = new XMLHttpRequest();
        req.open("GET", url);
        req.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(req.response)
            } else {
                reject({
                    status: this.status,
                    statusText: req.statusText
                });
            }
        }
        req.onerror = function () {
            reject({
                status: this.status,
                statusText: req.statusText
            });
        };

        req.send();
    });
}

//---- MAINTASK-SUBTASK 02 ----
// myRequest(swUrl)
//     .then((response) => {
//         const data = processResponseText(response);
//         starWarsRenderFunction(data);
//     })
//     .catch((error) => {
//         console.log(error);
//     })

//---- MAINTASK-SUBTASK 03 ----
// fetch(swUrl)
//     .then((response) => response.json())
//     .then(function(json) {
//         const data = processJson(json.results);
//         starWarsRenderFunction(data);
//     })
//     .catch((error) => {
//         console.log(error);
//     })


//---- MAINTASK-SUBTASK 04 ----
fetchFunction(swUrl)
    .then((responseJson) => {
        let processData = processJson(responseJson.results);
        starWarsRenderFunction(processData);
    });

async function fetchFunction(url) {
    let data = await fetch(url);
    let jsonData = await data.json();
    return jsonData;
}


//---- BONUS TASK 01 ----
const socket = new WebSocket('ws://salty-peak-74076.herokuapp.com/');
const messageArea = document.querySelector('#chat');
const inputArea = document.querySelector('#chat-input');
console.log(inputArea);

inputArea.addEventListener('keyup', function (e) {
    if (inputArea.focus) {
        // console.log(e);
        if (e.key == "Enter") {
            const myMessage = inputArea.value;
            socket.send(myMessage);
            console.log(`sending message ${myMessage}`);
            inputArea.value = '';
        }
    }
})

socket.addEventListener('open', function (event) {
    console.log('connection established');
    // socket.send('Hello Server!');
});

socket.addEventListener('message', function (event) {
    messageArea.innerHTML += event.data + '\n';
    console.log(`Message from ${event.origin}: ${event.data}`);
});


//---- BONUS TASK 02 ----
const mapyUrl = "https://api.mapy.cz/geocode?query=praha";

function mapsRenderFunction(parsedData) {
    const mapEl = document.querySelector('#mapy');

    for(const result of parsedData){
        console.log(result);
        const newLi = document.createElement('li');
        const subUl = document.createElement('ul');
        newLi.appendChild(subUl);

        const x = result.getAttribute('x');
        const y = result.getAttribute('y');
        const title = result.getAttribute('title');
        const values = {title,x,y};
        for(const [key, value] of Object.entries(values)){
            const subLi = document.createElement('li');
            subLi.innerHTML = `${key}: ${value}`;
            subLi.id = key;
            subUl.appendChild(subLi);
        }

        mapEl.appendChild(newLi);
    }
}

function proccessXMLresponse(response){
    const parser = new DOMParser();
    data = parser.parseFromString(response, "text/xml");
    return data.getElementsByTagName('item');
}

myRequest(mapyUrl)
    .then((response) => {
        const data = proccessXMLresponse(response);
        mapsRenderFunction(data);
    })
    .catch((error) => {
        console.log(error);
    })



/*
    urls:
        HTTP API with JSON response of StarWars characters
            https://swapi.co/api/people/
        WebSocket API with text messages
            ws://salty-peak-74076.herokuapp.com/
        HTTP API with XML response of places matching given query
            https://api.mapy.cz/geocode?query=praha

    Main tasks
        1. Use XMLHttpRequest to create HTTP request and get data from StarWarsApi
            1. Parse StarWars data
            2. Transform the data into list of character names
                ["Luke Skywalker", "C-3PO", ...]
            3. Render character names into the list - <ul id="star-wars"></ul>
        2. Create function that fetches data with XMLHttpRequest but exposes Promises interface.
            myRequest(url)
                .then(response => {
                    // ok state
                })
                .catch(error => {
                    // error states
                })

            1. For rendering the Star-Wars characters - switch from XMLHttpRequest to your "myRequest" function
        3. Try to use browser's fetch API
            1. For rendering the Star-Wars characters - try to switch to browsers fetch API
        4. Try to use async/await for interaction with
            1. myRequest
            2. fetch

    Bonus Tasks
        1. Websocket chat
            1. Connect to WebSocket API
            2. When messages comes in, render it to the - <textarea id="chat">
            3. When user types in text in <input type="text" id="chat-input"> and presses enter, send the message to the chat
        2. Mapy.cz API with XML response
            1. Fetch the data, the idea is the same, except we get XML instead of JSON as a response
*/