//Document Elements
const PLAYER_INPUT = document.querySelector('#command-input');
const RESPONSE_BOX = document.querySelector('#response-box');
const DIFFICULTY_SLIDER = document.querySelector('#difficulty-slider');
const NEW_GAME = document.querySelector('#new-game-button');
const TOUCH_INPUT_CONTAINER = document.querySelector('.touch-input-container');
const TOUCH_CATEGORY_TOGGLE_CONTAINER = document.querySelector('.category-toggle-container');
const TOUCH_INPUT_CHECKBOX = document.querySelector('#touch-checkbox-input');

PLAYER_INPUT.addEventListener('keyup', function (e) {
    if (PLAYER_INPUT.focus) {
        if (e.key === 'Enter') {
            const inputText = PLAYER_INPUT.value;
            if (inputText !== '') {
                addTextToBox('> ' + inputText);
                PLAYER_INPUT.value = '';
                if (typeof game !== 'undefined') {
                    game.handleCommand(inputText);
                } else {
                    alert('Please reload the game. Something broke down. ' +
                        '\nIf the issue persist please contact the developer.');
                }
                addTextToBox('');
            }
        }
    }
});

NEW_GAME.addEventListener('click', function () {
    const difficulty = DIFFICULTY_SLIDER.value;
    const maxDif = DIFFICULTY_SLIDER.max;
    if (confirm(`Do you want to start a new game?\n Chosen difficulty: ${difficulty} / ${maxDif}`)) {
        game = new Game(difficulty);
    }
    // game.printGrid();
});


function resetResponseBox() {
    RESPONSE_BOX.innerHTML = '';
}

function addTextToBox(string) {
    RESPONSE_BOX.innerHTML += string + '\n';
    RESPONSE_BOX.scrollTop = RESPONSE_BOX.scrollHeight;
}


//GAME 'ENGINE'
class Game {
    constructor(difficulty, size = 10) {
        resetResponseBox();
        this.size = size;
        this.grid = createArray(size, size);
        this.roomCount = Math.floor((size * size) / 5);

        //generating random rooms
        const position = this.travelerRoomFiller(this.roomCount);
        this.roomCount = this.grid.flat().filter(a => a !== undefined).length;

        this.winGoldCount = this.roomCount * difficulty;

        this.player = new Player(position, this);
        // console.log(this.player.position);
        // this.randomRoomFiller();
        addTextToBox(`Welcome adventurer! ` +
            `\n Your goal is to collect ${this.winGoldCount} pieces of GOLD. ` +
            `You can hustle, kill, steal or be nice to everyone but everything comes with a price. ` +
            `Once you finish you journey you will be crowned the king of this world. ` +
            `\n  Your start with ${this.player.hp} HP and empty pockets. ` +
            `\n  Good luck \n`);
    }

    //general for creating new Command instance and then resolving action
    handleCommand(string) {
        //fixing touch input
        if (string[string.length - 1] === ' ') {
            string = string.substring(0, string.length - 1);
        }

        const command = new Command(string, this);
        if (command.isValid()) {
            // console.log(command);
            command.act();
        } else {
            addTextToBox('Command is not valid');
        }
    }

    //--ACTING IN GAME WORLD--
    //most of them are for player interaction

    //checking if move is possible then moving or not
    movePlayer(position, direction) {
        const room = this.getRoom(this.player.position);
        const enemy = room.enemyOnPath(direction);
        if (enemy === undefined) {
            if (this.isPositionInBound(position)) {
                if (this.isThereRoom(position)) {
                    addTextToBox('You have moved to desired location');
                    this.player.setPosition(position);
                } else {
                    addTextToBox('There is nothing but emptiness and despair');
                }
            } else {
                addTextToBox('Seems like you have traveled to the end of the world');
            }
        } else {
            addTextToBox(`There is ${enemy} blocking further travel`);
        }
    }

    pickUpItem(item) {
        const pos = this.player.position;
        const room = this.grid[pos.x][pos.y];
        if (room.items.has(item)) {
            const itemInst = room.items.get(item);
            this.player.addItem(itemInst);
            room.items.delete(item);
            addTextToBox(`You have picked up a ${item}`);
        } else {
            addTextToBox(`There is not a ${item} nowhere near you`);
        }
    }

    giveItem(item, receiver) {
        const room = this.getRoom(this.player.position);
        const char = this.getCharInRoom(receiver, room);
        if (char !== undefined) {
            const response = this.player.giveItem(item);
            if (response) {
                addTextToBox(`You gave up your ${item} to ${receiver}`);
                const response = char.receiveItem(item, this);
                addTextToBox(response.message);
            } else {
                addTextToBox(`You carry no ${item} in you pockets`);
            }
        } else {
            addTextToBox(`There is no ${receiver} near you.`)
        }
    }

    useItem(item) {
        const response = this.player.useItem(item);
        addTextToBox(response.message);
    }

    hitSomeone(target, attacker) {
        // console.log(attacker);
        // debugger;
        const damage = attacker.rollDamage();
        if (attacker === this.player) {
            const room = this.getRoom(this.player.position);
            const targetChar = this.getCharInRoom(target, room);
            if (targetChar !== undefined) {
                addTextToBox(`You have hit ${target} for ${damage} damage`);
                const responseAction = targetChar.getHit(damage, this);
                // room.enemies.get(target).getHit(damage, this);
                responseAction();
                // } else if (room.neutrals.has(target)) {
                //     room.neutrals.get(target).getHit(damage, this);
                //     addTextToBox(`You have hit ${target} for ${damage} damage`);
            } else {
                addTextToBox(`There is no ${target} in this ${room.env.name}`);
            }
        } else {
            addTextToBox(`${attacker.name} hit you for ${damage} damage`);
            this.player.getHit(damage, this);
        }
    }

    talkToSomeone(target) {
        const pos = this.player.position;
        const room = this.getRoom(pos);
        const char = this.getCharInRoom(target, room)
        if (char !== undefined) {
            const response = char.behaviour.talk();
            addTextToBox(`${target}: ${response}`);
        } else {
            addTextToBox(`There is not a ${target} nowhere near you`);
        }
    }

    showInventory() {
        const desc = this.player.getInventoryDesc();
        const prefix = 'Inventory: ';
        addTextToBox(prefix + desc);
    }

    lookAround() {
        const pos = this.player.position;
        const desc = this.grid[pos.x][pos.y].toString();
        addTextToBox(desc);
    }


    //--GAME STATE CHANGING--

    gameOver() {
        addTextToBox('');
        addTextToBox('YOU DIED - next journey might be the ONE');
        PLAYER_INPUT.disabled = true;
    }

    gameWin() {
        addTextToBox('');
        addTextToBox('YOU WON - may your legend live forever');
        PLAYER_INPUT.disabled = true;
    }

    checkWinCondition() {
        if (this.player.inventory.has('GOLD')) {
            const goldCount = this.player.inventory.get('GOLD').uses;
            if (goldCount >= this.winGoldCount) {
                this.gameWin();
            }
        }
    }


    //NPC died player gets reward
    die(char) {
        const goldCount = char.character.value.behaviour.die();
        addTextToBox(`You have ${char.enemy ? 'defeated' : 'murdered'} ${char.name}. Looted the corpse for ${goldCount}x GOLD.`);

        this.removeNPCFromPlayerRoom(char);

        const gold = new Item('GOLD');
        for (let i = 0; i < goldCount; i++) {
            // console.log(`adding gold`);
            this.player.addItem(gold);
        }
    }

    //NPC left the world without reward
    leave(char) {
        addTextToBox(`${char.name} has left this room.`);
        this.removeNPCFromPlayerRoom(char);
    }

    removeNPCFromPlayerRoom(char) {
        const room = this.getRoom(this.player.position);
        if (room.enemies.has(char.name)) {
            room.enemies.delete(char.name);
        } else {
            room.neutrals.delete(char.name);
        }
    }

    //GENERATING ROOMS IN WORLD
    travelerRoomFiller(roomCount) {
        let x = Math.floor(this.size / 2);
        let y = x;

        while (roomCount > 0) {
            const direction = getRandomInt(4);
            let newPos = {x, y};
            switch (direction) {
                case 0: {
                    newPos = {x, y: y + 1};
                    break;
                }
                case 1: {
                    newPos = {x: x + 1, y};
                    break;
                }
                case 2: {
                    newPos = {x, y: y - 1};
                    break;
                }
                case 3: {
                    newPos = {x: x - 1, y};
                    break;
                }
            }

            if (this.isPositionInBound(newPos)) {
                if (!this.isThereRoom(newPos)) {
                    x = newPos.x;
                    y = newPos.y;
                    this.grid[x][y] = new Room({x, y}, this);

                    if (this.isTrapped({x, y})) {
                        break;
                    }
                    roomCount -= 1;
                }
            }
        }
        return {x, y};
    }

    //for rooms generating travelerRoomFiller()
    isTrapped(pos) {
        for (let x = -1; x < 2; x++) {
            for (let y = -1; y < 2; y++) {
                let curPosition = {x: pos.x + x, y: pos.y + y};
                if (this.isPositionInBound(curPosition)) {
                    if (!this.isThereRoom(curPosition)) {
                        return false;
                    }
                }
            }
        }
        return true;
    }

    //unused generating rooms in world
    randomRoomFiller() {
        for (let x = 0; x < this.size; x++) {
            for (let y = 0; y < this.size; y++) {
                const createRoom = getRandomInt(10) > 5;
                if (createRoom) {
                    this.grid[x][y] = new Room({x, y}, this);
                }
            }
        }
    }

    isPositionInBound(pos) {
        return pos.x < this.size
            && pos.x >= 0
            && pos.y < this.size
            && pos.y >= 0;
    }

    isThereRoom(pos) {
        return this.getRoom(pos) !== undefined
    }

    getRoom(pos) {
        return this.grid[pos.x][pos.y];
    }

    getCharInRoom(receiver, room) {
        if (room.enemies.has(receiver)) {
            return room.enemies.get(receiver);
        } else if (room.neutrals.has(receiver)) {
            return room.neutrals.get(receiver);
        }
        return undefined;
    }

    //DEV ONLY
    printGrid() {
        for (let y = this.size - 1; y >= 0; y--) {
            let string = '';
            for (let x = 0; x < this.size; x++) {
                if (this.isThereRoom({x, y})) {
                    string += `[${x},${y}]`;
                } else {
                    string += `[ , ]`;
                }
            }
            console.log(string);
        }
    }
}

//PLAYER CHARACTER
class Player {
    constructor(pos, game) {
        this.inventory = new Map();
        this.hp = 5;
        this.position = pos;
        this.game = game;
    }

    setPosition(newPos) {
        this.position.x = newPos.x;
        this.position.y = newPos.y;
    }

    giveItem(item) {
        if (this.inventory.has(item)) {
            const i = this.inventory.get(item);
            i.addUses(-1);
            if (i.uses <= 0 || item === 'SWORD') {
                this.inventory.delete(item);
            }
            return true;
        } else {
            return false;
        }
    }

    addItem(item) {
        // console.log('adding item');
        // console.log(item);
        if (this.inventory.has(item.name)) {
            const i = this.inventory.get(item.name);
            i.addUses(item.uses);
        } else {
            this.inventory.set(item.name, item);
        }

        if (item.name === 'GOLD') {
            this.game.checkWinCondition();
        }
    }

    useItem(item, combat = false) {
        if (this.inventory.has(item)) {
            const i = this.inventory.get(item);
            const message = i.use(this, combat);
            if (i.uses === 0) {
                this.inventory.delete(item);
            }
            return {result: true, message};
        } else {
            return {result: false, message: `You carry no ${item} in you pockets`};
        }
    }


    //intetory to text
    getInventoryDesc() {
        let desc = '';
        if (this.inventory.size <= 0) {
            return 'nothing in your pockets';
        }
        this.inventory.forEach((v, k) => {
            // console.log(v);
            // console.log(k);
            desc += `\n   ${v.toString()}`;
        });
        return desc;
    }

    getHit(damage, game) {
        if ((this.hp - damage) <= 0) {
            game.gameOver();
        } else {
            this.hp -= damage;
        }
    }

    rollDamage() {
        let damage;
        if (this.useItem('SWORD', true).result) {
            damage = getRandomInt(5, 3);
        } else {
            damage = getRandomInt(2, 1);
        }
        // console.log(`You have rolled ${damage}`);
        return damage;
    }
}


//COMMAND HANDLING CLASS
class Command {
    constructor(string, game) {
        this.valid = true;
        this.game = game;
        string = string.toUpperCase();
        const parsed = string.split(' ');
        this.actionString = parsed[0];
        if (!ACTIONS.includes(this.actionString)) {
            this.valid = false;
            return;
        }
        switch (this.actionString) {
            case 'MOVE': {
                if (parsed.length === 2) {
                    this.direction = parsed[1];

                    if (DIRECTIONS.has(this.direction)) {
                        const newPos = {x: this.game.player.position.x, y: this.game.player.position.y};
                        switch (this.direction) {
                            case 'NORTH': {
                                newPos.y += 1;
                                break;
                            }
                            case 'EAST': {
                                newPos.x += 1;
                                break;
                            }
                            case 'SOUTH': {
                                newPos.y -= 1;
                                break;
                            }
                            case 'WEST': {
                                newPos.x -= 1;
                                break;
                            }
                        }

                        // console.log('Player position:')
                        // console.log(this.game.player.position);
                        // console.log(newPos);

                        this.act = () => {
                            this.game.movePlayer(newPos, this.direction);
                        };
                        break;
                    }
                }
                this.valid = false;
                break;
            }
            case 'LOOK': {
                if (parsed.length === 1) {
                    this.act = () => {
                        this.game.lookAround();
                    };
                    break;
                }
                this.valid = false;
                break;
            }
            case 'HIT': {
                if (parsed.length === 2) {
                    this.target = parsed[1];
                    this.act = () => {
                        this.game.hitSomeone(this.target, this.game.player);
                    };
                    break;
                }
                this.valid = false;
                break;
            }
            case 'TALK': {
                if (parsed.length === 3) {
                    this.junction = parsed[1];
                    if (PREPOSITIONS.includes(this.junction)) {
                        if (this.junction === 'TO') {
                            this.target = parsed[2];
                            this.act = () => {
                                this.game.talkToSomeone(this.target);
                            };
                            break;
                        }
                    }
                }
                this.valid = false;
                break;
            }
            case 'INVENTORY': {
                if (parsed.length === 1) {
                    this.act = () => {
                        this.game.showInventory();
                    };
                    break;
                }
                this.valid = false;
                break;
            }
            case 'PICK': {
                if (parsed.length === 3) {
                    this.junction = parsed[1];
                    if (PREPOSITIONS.includes(this.junction)) {
                        if (this.junction === 'UP') {
                            this.item = parsed[2];
                            this.act = () => {
                                this.game.pickUpItem(this.item);
                            };
                            break;
                        }
                    }
                }
                this.valid = false;
                break;
            }
            case 'USE': {
                if (parsed.length === 2) {
                    this.item = parsed[1];
                    this.act = () => {
                        this.game.useItem(this.item);
                    };
                    break;
                }
                this.valid = false;
                break;
            }
            case 'GIVE': {
                if (parsed.length === 4) {
                    this.item = parsed[1];
                    if (parsed[2] === 'TO') {
                        this.receiver = parsed[3];
                        this.act = () => {
                            this.game.giveItem(this.item, this.receiver);
                        };
                        break;
                    }
                }
                this.valid = false;
                break;
            }
            default: {
                this.valid = false;
            }
        }
    }

    isValid() {
        return this.valid;
    }

    //THIS METHOD SHOULD BE OVERRIDEN WHEN COMMAND IS HANDLED
    act() {
        console.log('UNCHANGED ACT METHOD IN COMMAND');
    }
}

//ROOM
class Room {
    constructor(position, game) {
        //gets random environment
        this.env = new Environment();
        this.game = game;
        this.x = position.x;
        this.y = position.y;

        //setup based on given environment
        this.items = this.env.generateItems();
        this.enemies = this.env.generateEnemies();
        this.neutrals = this.env.generateNeutrals();
    }

    enemyOnPath(direction) {
        for (let [k, v] of this.enemies.entries()) {
            if (v.position === direction) {
                return k;
            }
        }
        return undefined;
    }

    toString() {
        let desc = this.env.name + ':';
        desc += "\n   enemies: ";
        this.enemies.forEach(e => desc += e.name + ", ");
        desc += "\n   neutrals: ";
        this.neutrals.forEach(e => desc += e.name + ", ");
        desc += "\n   items: ";
        this.items.forEach(e => desc += e.name + ", ");
        return desc;
    }

}

class Environment {
    constructor(name = null) {
        if (name == null) {
            this.env = getRandomMapEntry(ENVIRONMENTS);
        }
        this.name = this.env.key;
    }

    generateItems() {
        return this.env.value.generateItems();
    }

    generateEnemies() {
        return this.env.value.generateEnemies();
    }

    generateNeutrals() {
        return this.env.value.generateNeutrals();
    }
}

class EnvironmentStrategy {
    constructor(itemCount = 1, enemyCount = 1, neutralCount = 1) {
        this.itemCount = itemCount;
        this.enemyCount = enemyCount;
        this.neutralCount = neutralCount;
    }

    generateItems() {
        const items = new Map();
        let itemCount = this.itemCount;
        while (itemCount > 0) {
            const newItem = new Item();
            if (!items.has(newItem.name)) {
                items.set(newItem.name, newItem);
            }
            itemCount -= 1;
        }
        return items;
    }

    generateEnemies() {
        const enemies = new Map();
        let enemyCount = this.enemyCount;
        while (enemyCount > 0) {
            const newNPC = new NPC(true);
            if (!enemies.has(newNPC.name)) {
                enemies.set(newNPC.name, newNPC);
            }
            enemyCount -= 1;
        }
        return enemies;
    }

    generateNeutrals() {
        const neutrals = new Map();
        let neutralCount = this.neutralCount;
        while (neutralCount > 0) {
            // debugger;
            const newNPC = new NPC(false);
            if (!neutrals.has(newNPC.name)) {
                neutrals.set(newNPC.name, newNPC);
            }
            neutralCount -= 1;
        }
        return neutrals;
    }
}

class ForestStrategy extends EnvironmentStrategy {
    constructor(itemCount = 2, enemyCount = 3, neutralCount = 1) {
        super(itemCount, enemyCount, neutralCount);
    }
}

class SwampStrategy extends EnvironmentStrategy {
    constructor(itemCount = 1, enemyCount = 4, neutralCount = 1) {
        super(itemCount, enemyCount, neutralCount);
    }
}

class FieldStrategy extends EnvironmentStrategy {
    constructor(itemCount = 2, enemyCount = 1, neutralCount = 1) {
        super(itemCount, enemyCount, neutralCount);
    }
}

class Item {
    constructor(name = null) {
        if (name == null) {
            // let index = getRandomInt(ITEMS.length);
            // name = ITEMS.get('GOLD');
            this.item = getRandomMapEntry(ITEMS);
        } else {
            this.item = {key: name, value: ITEMS.get(name)};
        }
        this.name = this.item.key;
        this.uses = this.item.value.uses;
        this.strategy = this.item.value.strategy;
        this.consumable = this.item.value.consumable;
    }

    addUses(amount) {
        // console.log(`adding ${amount} to ${this.name} with ${this.uses} uses`);
        this.uses += amount;
        // console.log(`final uses: ${this.uses}`);
    }

    use(player, combat = false) {
        let message = this.strategy.use(player, combat);
        if (this.consumable) {
            this.addUses(-1);
            if (this.uses <= 0) {
                message += `\nYou have used up your ${this.name}s`;
            }
        }
        return message;
    }

    toString() {
        return `${this.name} : ${this.uses}x`;
    }
}

class GoldStrategy {
    constructor() {
    }

    use(player, combat = false) {
        const head = 50 < getRandomInt(100);
        return `You have flipped ${head ? 'head' : 'tail'}.`;
    }
}

class KeyStrategy {
    constructor() {
    }

    use(player, combat = false) {
        return `I wonder what it is from?? Hmmm.`;
    }
}

class PotionStrategy {
    constructor() {
    }

    use(player, combat = false) {
        const weird = 10 < getRandomInt(100);
        let message = '';
        if (weird) {
            message = 'You feel kind of weird, but nothing else happened.'
        } else {
            const hpBoost = getRandomInt(5, 1);
            player.hp += hpBoost;
            message = `You have healed up for ${hpBoost}.`;
        }
        return message;
    }
}

class SwordStrategy {
    constructor() {
    }

    use(player, combat = false) {
        const clumsy = 70 < getRandomInt(100);
        let message = ''
        if (clumsy) {
            message = 'You have dropped your sword and it got all dirty. :('
        } else {
            message = 'You have flipped the sword behind you back and caught it on your finger. Very impressive indeed. Clap clap clap.'
        }
        return message;
    }
}


class NPC {
    constructor(enemy = false, name = null) {
        this.enemy = enemy;
        this.position = getRandomMapEntry(DIRECTIONS).key;
        if (name == null) {
            if (enemy) {
                this.character = getRandomMapEntry(ENEMIES);
            } else {
                this.character = getRandomMapEntry(NEUTRALS);
            }
        }
        this.name = this.character.key;
        this.hp = getRandomInt(this.character.value.hpMax) + 1;
        this.behaviour = this.character.value.behaviour.getNewBehaviour();
        // console.log(this.behaviour);
    }

    getHit(damage, game) {
        // console.log(`HP: ${this.hp} > damage: ${damage}`);
        if ((this.hp - damage) <= 0) {
            return () => game.die(this);
        } else {
            this.hp -= damage;
            return () => game.hitSomeone(game.player, this);
        }
    }

    receiveItem(item, game) {
        const response = this.behaviour.receiveItem(item, game);
        if (response.action !== undefined) {
            response.action(this);
        }
        return response;
    }

    rollDamage() {
        // console.log(`${this.name} rolled ${damage}`);
        return getRandomInt(this.character.value.damage, 1);
    }
}


//NPC BEHAVIOURS
class Behaviour {
    constructor(dialogs, damage, dropMax) {
        // console.log(`dmg: ${damage} | dropMax: ${dropMax}`);
        this.dialogs = dialogs;
        this.damage = damage;
        this.dropMax = dropMax;
        this.dropGold = getRandomInt(this.dropMax + 1, 1);
    }

    attack() {
        return getRandomInt(this.damage + 1);
    };

    talk() {
        return this.dialogs[getRandomInt(this.dialogs.length)];
    };

    receiveItem(item, game) {
        console.log('UNCHANGED RECEIVE ITEM');
    };

    die() {
        return this.dropGold;
    };
}

class WolfBehaviour extends Behaviour {
    constructor(damage, dropMax) {
        const dialogs = ['Woof', 'Howl'];
        super(dialogs, damage, dropMax);
    }

    receiveItem(item, game) {
        if (item === 'POTION') {
            return {message: 'Howl......', action: (char) => game.die(char)};
        } else {
            return {message: 'Woof!! Woof!'};
        }
    }

    //to have random gold drop count
    getNewBehaviour() {
        return new WolfBehaviour(this.damage, this.dropMax);
    };
}

class BanditBehaviour extends Behaviour {
    constructor(damage, dropMax) {
        const dialogs = [
            'Give me all your money!!!',
            'Life or gold. Your choice. oX'
        ];
        super(dialogs, damage, dropMax);
    }

    receiveItem(item, game) {
        if (item === 'GOLD' || item === 'SWORD') {
            return {message: 'Bandit have peacefully left this area.', action: (char) => game.leave(char)};
        } else {
            return {message: 'Are you making joke of me?'};
        }
    }

    //to have random gold drop count
    getNewBehaviour() {
        return new BanditBehaviour(this.damage, this.dropMax);
    };
}

class TravelerBehaviour extends Behaviour {
    constructor(damage, dropMax) {
        const dialogs = [
            'I have journeyed many worlds but this one seems particularly interesting.',
            'Traveling is my life.',
            'I feel weak. Vital boost would be great.'
        ];
        super(dialogs, damage, dropMax);
    }

    receiveItem(item, game) {
        if (item === 'POTION') {
            return {
                message: 'That is what i needed. Here have my sword as a thanks.',
                action: () => game.player.addItem(new Item('SWORD'))
            };
        } else {
            return {message: 'Thank you for you precious gift.'};
        }
    }

    //to have random gold drop count
    getNewBehaviour() {
        return new TravelerBehaviour(this.damage, this.dropMax);
    };
}

class MonkBehaviour extends Behaviour {
    constructor(damage, dropMax) {
        const dialogs = [
            'Bless you.',
            'Some paths on our journeys are locked some are not.',
            'Our church needs every help it can get.'
        ];
        super(dialogs, damage, dropMax);
    }

    receiveItem(item, game) {
        if (item === 'KEY') {
            return {
                message: 'Is that THE KEY?... Here have some boost.',
                action: () => game.player.addItem(new Item('POTION'))
            };
        } else if (item === 'GOLD') {
            return {
                message: 'Let this blessing keep you on your feet.',
                action: () => game.player.hp += 2
            }
        } else {
            return {message: 'Thank you for you precious gift. But i do not have much of a use for it'};
        }
    }

    //to have random gold drop count
    getNewBehaviour() {
        return new MonkBehaviour(this.damage, this.dropMax);
    };
}

class ShopkeeperBehaviour extends Behaviour {
    constructor(damage, dropMax) {
        const dialogs = [
            'Welcome to my little shop.',
            'I will buy anything. But pricing may vary region by region.'
        ];
        super(dialogs, damage, dropMax);
    }

    receiveItem(item, game) {
        if (item === 'KEY') {
            return {
                message: 'Kind of rusty but still a nice pice of metal. Here have 1x GOLD',
                action: () => this.giveCoins(2, game)
            };
        } else if (item === 'GOLD') {
            return {
                message: 'Coin for coin. That is weird but deal is a deal',
                action: () => this.giveCoins(1, game)
            };
        } else if (item === 'SWORD') {
            return {
                message: 'That is beautiful craftsmanship. Here have 3x GOLD',
                action: () => this.giveCoins(3, game)
            };
        } else if (item === 'POTION') {
            return {
                message: 'Drug are hard to come by in this part of world. Here have 2x GOLD',
                action: () => this.giveCoins(2, game)
            };
        } else {
            return {message: 'Let me have a look on it.'};
        }
    }

    giveCoins(amount, game) {
        while (amount > 0) {
            game.player.addItem(new Item('GOLD'));
            amount -= 1;
        }
    }

    //to have random gold drop count
    getNewBehaviour() {
        return new ShopkeeperBehaviour(this.damage, this.dropMax);
    }
}


//UTILS FOR GAME CREATING
function getRandomMapEntry(map) {
    let index = getRandomInt(map.size);
    for (let key of map.keys()) {
        if (index <= 0) {
            return {key, value: map.get(key)};
        }
        index--;
    }
}

function getRandomInt(max, min = 0) {
    return Math.max(Math.floor(Math.random() * Math.floor(max)), min);
}

function createArray(length) {
    let arr = new Array(length || 0),
        i = length;

    if (arguments.length > 1) {
        let args = Array.prototype.slice.call(arguments, 1);
        while (i--) arr[length - 1 - i] = createArray.apply(this, args);
    }

    return arr;
}


//DATA SETS DECLARATION
const ENVIRONMENTS = new Map([
    ['FOREST', new ForestStrategy()],
    ['SWAMP', new SwampStrategy()],
    ['FIELD', new FieldStrategy()]
]);
const ITEMS = new Map([
    ['GOLD', {uses: 1, consumable: false, strategy: new GoldStrategy()}],
    ['KEY', {uses: 1, consumable: false, strategy: new KeyStrategy()}],
    ['SWORD', {uses: 10, consumable: true, strategy: new SwordStrategy()}],
    ['POTION', {uses: 1, consumable: true, strategy: new PotionStrategy()}]
]);
const ENEMIES = new Map([
    ['WOLF', {hpMax: 2, damage: 2, dropMax: 1, behaviour: new WolfBehaviour(2, 1)}],
    ['BANDIT', {hpMax: 3, damage: 3, dropMax: 3, behaviour: new BanditBehaviour(3, 3)}]
]);
const NEUTRALS = new Map([
    ['MONK', {hpMax: 5, damage: 0, dropMax: 0, behaviour: new MonkBehaviour(0, 0)}],
    ['TRAVELER', {hpMax: 15, damage: 5, dropMax: 2, behaviour: new TravelerBehaviour(5, 2)}],
    ['SHOPKEEPER', {hpMax: 20, damage: 8, dropMax: 5, behaviour: new ShopkeeperBehaviour(8, 5)}]
]);

const ACTIONS = ['MOVE', 'PICK', 'USE', 'TALK', 'GIVE', 'HIT', 'LOOK', 'INVENTORY'];
const PREPOSITIONS = ['UP', 'TO'];
const DIRECTIONS = new Map([
    ['NORTH', 0],
    ['EAST', 1],
    ['SOUTH', 2],
    ['WEST', 3]
]);


//--HTML ELEMENTS SETUP--

//SLIDER EFFECT
DIFFICULTY_SLIDER.addEventListener('input', () => {
    document.documentElement.style
        .setProperty('--slider-value', ((1 + DIFFICULTY_SLIDER.value / 15) + 'rem'));
});

document.documentElement.style
    .setProperty('--slider-value', ((1 + DIFFICULTY_SLIDER.value / 15) + 'rem'));


//INIT TOUCH INPUTS
//checkbox scroll to element
TOUCH_INPUT_CHECKBOX.addEventListener('click', () => {
    if (TOUCH_INPUT_CHECKBOX.checked) {
        TOUCH_INPUT_CONTAINER.scrollIntoView();
    }
})

//add actions
const actionsContainer = document.createElement('div');
actionsContainer.classList.add('category-container');
TOUCH_INPUT_CONTAINER.insertBefore(actionsContainer, TOUCH_INPUT_CONTAINER.firstChild);
for (const item of ACTIONS) {
    const newEl = createTouchInput(item);
    actionsContainer.appendChild(newEl);
}

//add all categories touch-inputs
const allInputs = [
    {name: 'PREPOSITIONS', array: PREPOSITIONS},
    {name: 'ITEMS', array: Array.from(ITEMS.keys())},
    {name: 'DIRECTIONS', array: Array.from(DIRECTIONS.keys())},
    {name: 'ENEMIES', array: Array.from(ENEMIES.keys())},
    {name: 'NEUTRALS', array: Array.from(NEUTRALS.keys())}
];

for (const category of allInputs) {
    // console.log(category.name);
    const categoryContainer = document.createElement('div');
    categoryContainer.classList.add('touch-input-category');
    categoryContainer.id = category.name;
    TOUCH_INPUT_CONTAINER.appendChild(categoryContainer);
    createCategoryToggle(category.name);
    for (const item of category.array) {
        categoryContainer.appendChild(createTouchInput(item));
    }
}

//select some list of other commands
const el = document.querySelector("[name='category-toggle']");
el.checked = true;

function createTouchInput(name) {
    const newEl = document.createElement('div');
    newEl.classList.add('touch-input');
    newEl.innerText = name;
    newEl.addEventListener('click', () => {
        PLAYER_INPUT.value += name + ' ';
    });
    return newEl;
}

function createCategoryToggle(name) {
    let styleEl = document.createElement('style');

    // Append <style> element to <head>
    document.head.appendChild(styleEl);

    // Grab style element's sheet
    let styleSheet = styleEl.sheet;

    styleSheet.insertRule(`#${name}` + '{display: none;}', 0);
    styleSheet.insertRule(`#${name}-toggle:checked ~ #${name}` + '{display: flex;}', 0);
    styleSheet.insertRule(`#${name}-toggle:checked ~ .category-toggle-container > #${name}-label` + '{background-color: var(--chosen-button);}', 0);


    // console.log(styleSheet.cssRules);

    // addStylesheetRules([
    //         [`#${name}`, ['display: none;']],
    //         [`#${name}-toggle:checked ~ #${name}`, ['display: block;']]
    //     ]
    // )
    const newRadio = document.createElement('input');
    newRadio.type = 'radio';
    newRadio.id = `${name}-toggle`;
    // newRadio.classList.add('category-toggle-radio');
    newRadio.name = 'category-toggle';
    const newLabel = document.createElement('label');
    newLabel.setAttribute('for', newRadio.id);
    newLabel.innerText = name;
    newLabel.id = `${name}-label`;
    newLabel.classList.add('touch-input');
    TOUCH_INPUT_CONTAINER.insertBefore(newRadio, TOUCH_INPUT_CONTAINER.firstChild);
    TOUCH_CATEGORY_TOGGLE_CONTAINER.appendChild(newLabel);
}


//RUN DEL BUTTONS
const runDelContainer = document.createElement('div');
runDelContainer.classList.add('touch-execute-container');
TOUCH_INPUT_CONTAINER.appendChild(runDelContainer);

const newEl = document.createElement('div');
newEl.classList.add('touch-input');
newEl.innerText = 'RUN';
newEl.addEventListener('click', () => {
    const event = new KeyboardEvent('keyup', {key: 'Enter'});
    PLAYER_INPUT.focus();
    PLAYER_INPUT.dispatchEvent(event);
    PLAYER_INPUT.blur();
});
runDelContainer.appendChild(newEl);

const newDel = document.createElement('div');
newDel.classList.add('touch-input');
newDel.innerText = 'DEL';
newDel.addEventListener('click', () => {
    PLAYER_INPUT.value = '';
});
runDelContainer.appendChild(newDel);


//STYLE HELPER
function addStylesheetRules(rules) {
    let styleEl = document.createElement('style');

    // Append <style> element to <head>
    document.head.appendChild(styleEl);

    // Grab style element's sheet
    let styleSheet = styleEl.sheet;

    for (let i = 0; i < rules.length; i++) {
        let j = 1,
            rule = rules[i],
            selector = rule[0],
            propStr = '';
        // If the second argument of a rule is an array of arrays, correct our variables.
        if (Array.isArray(rule[1][0])) {
            rule = rule[1];
            j = 0;
        }

        for (let pl = rule.length; j < pl; j++) {
            let prop = rule[j];
            propStr += prop[0] + ': ' + prop[1] + (prop[2] ? ' !important' : '') + ';\n';
        }

        // Insert CSS Rule
        styleSheet.insertRule(selector + '{' + propStr + '}', styleSheet.cssRules.length);
    }
}

//GAME INIT
let game = new Game(DIFFICULTY_SLIDER.value, 10);



